"""
Inseration Sort.
--------------

Insertion sort is a simple sorting algorithm that works the
way we sort playing cards in our hands.

Algorithm
// Sort an arr[] of size n
insertionSort(arr, n)
Loop from i = 1 to n-1.
……a) Pick element arr[i] and insert it into sorted sequence arr[0…i-1]

Time Complexity: O(n*2)
Auxiliary Space: O(1)
Best case :                                     𝛀(n)
"""

def InsertionSort(arr):

    # traverse through 1 to len(arr)
    for i in range(1, len(arr)):
        key = arr[i]

    # move elements of arr[0, .., n-1], that are greater
    # than key when position
        j = i - 1
        while j >= 0 and key < arr[j]:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key
    return arr


myList = [100, 20, 80, 30, 10, 50, 60, 40, 70, 90]
print("Elements before sorting : ", myList)

sorted = InsertionSort(myList)
print("Sorted list : ", sorted)
