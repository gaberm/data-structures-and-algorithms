'''
Binary Search

    It is the fastest searching technique.
    It is based on the Divide and Conquer method.
    But in this technique the elements of list must be in sorted order.
    Binary search begins by comparing the middle element of the list with the searching value.
    If the searching value matches the middle element, its position in the list is returned.
    And If the searching value is less than the middle element, the search continues in the lower half of the list.
    If the searching value is greater than the middle element, the search continues in the upper half of the list. By doing this, the algorithm eliminates the half in which the searching value cannot lie in each iteration.


## These two can be applied on both the best case and the worst case for binary search:

    best case: first element you look at is the one you are looking for
        Ω(1): you need at least one lookup
        Ο(1): you need at most one lookup
    worst case: element is not present
        Ω(log n): you need at least log n steps until you can say that the element you are looking for is not present
        Ο(log n): you need at most log n steps until you can say that the element you are looking for is not present

You see, the Ω and Ο values are identical. In that case you can say the tight bound for the best case is Θ(1) and for the worst case is Θ(log n).

But often we do only want to know the upper bound or tight bound as the lower bound has not much practical information.

Time Complexity - Upper bound time :            O(log n)
Best case :                                     𝛀(1)
'''

def BinarySearch(lys, val):
    first = 0
    last = len(lys)-1
    index = -1
    while (first <= last) and (index == -1):
        mid = (first+last)//2
        if lys[mid] == val:
            index = mid
        else:
            if val<lys[mid]:
                last = mid -1
            else:
                first = mid +1
    return index

myList = [10,20,30,40,50]

print("Elements in list : ", myList)

x = int(input("enter searching element :"))
result = BinarySearch(myList, x)

if result==-1:
    print("Element not found in the list.")
else:
    print("Element " + str(x) + " is found in position %d" %result)
