'''
Bubble Sort is the simplest sorting algorithm that works by repeatedly swapping the adjacent elements if they are in wrong order.
'''

def bubbleSort(arr):
    n = len(arr)
    steps=0
    # traverse array Elements
    for i in range(n):
        # print("i is: ",i)
        # last i elements are already in place - we don't care about
        for j in range(0, n-i-1):
            # print("j & j+1 are: ", j, j+1)
            #traverse the array from 0 to n-i
            # swap if element is found greater
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
            steps +=1

    return arr, steps

def bubbleSort2(arr):
    n = len(arr)
    steps = 0
    # traverse array Elements
    for i in range(n):
        # print("i is: ",i)
        ## swapped flag
        swapped = False
        # last i elements are already in place - we don't care about
        for j in range(0, n-i-1):
            # print("j & j+1 are: ", j, j+1)
            #traverse the array from 0 to n-i
            # swap if element is found greater
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
                swapped = True

            steps+=1

        # IF no two elements were swapped
        # by inner loop, then break
        if swapped == False:
            break

    return arr, steps


myList = [100,20,80,30,10,50,60,40,70,90]
print("Elements before sorting : ", myList)

sortedList, steps1 = bubbleSort(myList)
print("BubbleSort made {} steps", steps1)

sortedList, steps2 = bubbleSort2(myList)
print("BubbleSort made {} steps", steps2)

print("Elements after sorting : ", sortedList)
