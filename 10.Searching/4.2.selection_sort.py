"""
Selection Sort.
--------------

The selection sort algorithm sorts an array by repeatedly finding the minimum element (considering ascending order) from unsorted part and putting it at the beginning. The algorithm maintains two subarrays in a given array.
1) The subarray which is already sorted.
2) Remaining subarray which is unsorted.

In every iteration of selection sort, the minimum element (considering ascending order) from the unsorted subarray is picked and moved to the sorted subarray.
"""


import sys

A = [64, 25, 12, 22, 11]

for i in range(len(A)):

    min_idx = i

    for j in range(i + 1, len(A)):

        if A[min_idx] > A[j]:

            min_idx = j

    A[i], A[min_idx] = A[min_idx], A[i]

print("Sorted array")

for i in range(len(A)):

    print("%d" % A[i]),
