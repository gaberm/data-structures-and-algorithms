#!/usr/bin/env python
from exceptions import Empty

class LinkedDeque:

    class _Node:
        __slots__ = '_element', '_next'

        def __init__(self, element, next):
            self._element = element
            self._next = next

    def __init__(self):
        self._head = None
        self._tail = None
        self._size = 0

    def __len__(self):
        return self._size

    def is_empty(self):
        return self._size == 0

    def add_first(self, element):
        newNode = self._Node(element, None)
        if self.is_empty():
            self._head = newNode
            self._tail = newNode
        else:
            newNode._next = self._head
        self._tail = newNode
        self._size += 1

    def add_last(self, element):
        newNode = self._Node(element, None)
        if self.is_empty():
            self._head = newNode
            self._tail = newNode
        else:
            self._tail._next = newNode
        self._tail = newNode
        self._size += 1

    def remove_first(self):
        if self.is_empty():
            raise Empty('Stack is Empty')
        value = self._head._element
        self._head = self._head._next
        self._size = self._size - 1
        if self.is_empty():
            self._tail = None
        return value

    def remove_last(self):
        if self.is_empty():
            raise Empty('Stack is Empty')
        thead = self._head
        i = 0
        while i < len(self) - 2:
            thead = thead._next
            i += 1
        self._tail = thead
        thead = thead._next
        value = thead._element
        self._tail._next = None
        self._size -= 1
        return value

    def display(self):
        thead = self._head
        while thead :
            print(thead._element, end='->')
            thead = thead._next
        print()

ls = LinkedDeque()
ls.add_last(10)
ls.add_last(20)
ls.add_last(30)
ls.add_last(40)
ls.display()
print('Poppped: ', ls.remove_first())
ls.display()
ls.add_last(70)
ls.display()
print('Popped: ', ls.remove_last())
ls.display()
print(ls.__len__())
