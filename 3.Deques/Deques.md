## Deques
Deques work in the same manner as queues, except you can add as well as remove from the front as well as the rear



## Queues Abstract Data type

* Deque ADT stores objects
* Insert & Delete operations at both ends (Front & Rear)
* Operations:
  * Add_first(object): Insert at the start of the queue
  * Add_last(object): Insert at the end of the queue
  * Delete_First(object): deletes first element in the queue
  * Delete_Last(object): deletes last element in the queue
  * first(): returns element at the front
  * last(): returns element at the end
  * len(): returns number of elements
  * isEmpty(): whether queue is empty or not
