from exceptions import Empty

class DequeStack:
    def __init__(self):
        self._data = []
        self._size = 0
        self._front = 0
        self._last = 0

    def __len__(self):
        return self._size

    def is_empty(self):
        return self._size == 0

    def first(self):
        if self.is_empty():
            raise Empty("Queue is Empty")
        return self._data[self._front]

    def last(self):
        if self.is_empty():
            raise Empty("Queue is Empty")
        return self._data[self._last - 1]

    def add_last(self, e):
        self._data.append(e)
        self._size = self._size + 1
        self._last = self._last + 1

    def add_first(self, e):
        self._data.insert(self._front, e)
        self._size = self._size + 1
        self._last = self._last + 1

    def delete_first(self):
        if self.is_empty():
            raise Empty("Queue is Empty")
        value = self._data[self._front]
        self._data[self._front] = None
        self._front = self._front + 1
        self._size = self._size - 1
        return value

    def delete_last(self):
        if self.is_empty():
            raise Empty("Queue is Empty")
        value = self._data[-1]
        self._data.pop()
        self._size = self._size - 1
        return value

q = DequeStack()
q.add_last(10)
q.add_last(20)
print('Queue: ', q._data)
print('Is empty: ', q.is_empty())
print('First element: ', q.first())
print('Last element: ', q.last())
print('Lengh: ', q.__len__())
print('Deque: ', q._data)
q.add_first(30)
print('Deque: ', q._data)
q.delete_last()
print('Deque: ', q._data)
q.delete_first()
print('Deque: ', q._data)
