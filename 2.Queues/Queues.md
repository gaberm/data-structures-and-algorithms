## Queues

* Queue us a collection of objects
* First-in First-Out
* Fundamental operations are Enqueue & Dequeue
  * Enqueue(R)
  * Enqueue(R)
  * Dequeue()

* Queue Applications
  * Waiting queues
  * Access to shared resources

## Queues Abstract Data type

* Queue ADT stores objects
* FIFO scheme for Insert & Delete operations
* Insertion at rear of queue.
* Removal at front of queue.
* Operations:
  * enqueue(object): Insert at the end of the queue
  * dequeue(): remove & return at front of the queue
  * first(): returns element at the front
  * len(): returns number of elements
  * isEmpty(): whether queue is empty or not
