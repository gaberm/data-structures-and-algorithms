from exceptions import Empty

class QueueStack:
    def __init__(self):
        self._data = []
        self._size = 0
        self._front = 0

    def __len__(self):
        return self._size

    def is_empty(self):
        return self._size == 0

    def enqueue(self, e):
        self._data.append(e)
        self._size = self._size + 1

    def dequeue(self):
        if self.is_empty():
            raise Empty("Queue is Empty")
        value = self._data[self._front]
        self._data[self._front] = None
        self._front = self._front + 1
        self._size = self._size - 1
        return value

    def first(self):
        if self.is_empty():
            raise Empty("Queue is Empty")
        return self._data[self._front]

q = QueueStack()
q.enqueue(10)
q.enqueue(20)
print('Queue: ', q._data)
print('Is empty: ', q.is_empty())
print('First element: ', q.first())
print('Lengh: ', q.__len__())
print('Dequeue: ', q.dequeue())
print('Lengh: ', q.__len__())
print('Queue: ', q._data)


# q.

# x = [10, 20, 30]
