def BinarySearch(array, key):
    """
    Binary Search with recursive

    """

    if key < array[0] or key > array[-1]:
        return False

    mid = len(array) // 2
    left = array[:mid]
    right = array[mid:]

    if key == array[mid]:
        # print('bingo')
        return True
    elif key > array[mid]:
        BinarySearch(right, key)
    elif key < array[mid]:
        BinarySearch(left, key)
    else:
        return False

def binary_recursive(array, val):
    if val < array[0] or val > array[-1]:
        return False
    mid = len(array) // 2
    left = array[:mid]
    right = array[mid:]
    if val == array[mid]:
        return True
    elif array[mid] > val:
        return binary_recursive(left, val)
    elif array[mid] < val:
        return binary_recursive(right, val)
    else:
        return False



A = [4, 11, 18, 30, 54, 60]
# key = 11

BinarySearch(A, 30)
binary_recursive(A, 4)
