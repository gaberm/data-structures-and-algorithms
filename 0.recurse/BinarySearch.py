def BinarySearch(A, key):
    """
    Binary Search with a while loop

    """

    low = 0; high = len(A) - 1

    while low <= high:
        mid = (low + high) // 2
        if key == A[mid]:
            return True
        elif key < A[mid]:
            high = mid -1
        else:
            low = mid + 1
    return False



A = [4, 11, 18, 30, 54, 60]
# key = 11

BinarySearch(A, 60)
