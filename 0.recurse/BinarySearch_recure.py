def BinarySearch(A, key, low, high):
    """
    Binary Search with recursive

    """

    if low > high:
        return False
    else:
        mid = low+high //2
        if key == A[mid]:
            return True
        elif key < A[mid]:
            return BinarySearch(A, key, low, mid -1)
        else:
            return BinarySearch(A, key, mid + 1, high)
    return False



A = [4, 11, 18, 30, 54, 60]
# key = 11

BinarySearch(A, 70, 0, 5)
