## Stack ADT
* Stack ADT stores objects
* LIFO scheme for insert & delete operations
* Operations:
  * push(object): insert element
  * pop(): remove & return element
  * top(): returns last inserted element in the stack
  * len(): returns number of elements
  * isEmpty(): whether stack is empty or not
